﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class EndController : MonoBehaviour
{
  public TextMeshProUGUI loserText;

  private float resetTime = 3.5f;

  private void Start()
  {
    string loser = PlayerPrefs.GetString("Loser");
    loserText.text = loser + " DIDN'T MAKE IT ALIVE...";
  }

  private void Update()
  {
    if (Time.timeSinceLevelLoad >= resetTime)
    {
      ToMainMenu();
    }
  }

  public void ToMainMenu()
  {
    SceneManager.LoadScene("MainMenu");
  }
}
