﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
  public bool mute, muted = false;

  // Update is called once per frame
  void Update()
  {
    mute = Input.GetButtonDown("Mute");
    if (mute) muted = !muted;
    GetComponent<AudioSource>().enabled = !muted;
  }
}
