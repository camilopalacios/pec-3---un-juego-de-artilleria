﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

  private float verticalOffset = 3f;
  public Transform cameraObjective;

  // Update is called once per frame
  void FixedUpdate()
  {
    if (cameraObjective != null) transform.position = new Vector3(cameraObjective.position.x, cameraObjective.position.y + verticalOffset, transform.position.z);
  }

  public void SetCameraObjective(Transform objective)
  {
    cameraObjective = objective;
  }
}
