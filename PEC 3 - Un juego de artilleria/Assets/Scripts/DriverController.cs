﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DriverController : MonoBehaviour
{
  private bool facingRight = true;
  private float move;
  private float power = 50f;
  private float maxVelocity = 1f;
  private float bumperRadius = 0.2f;
  // TODO: make private
  public float life = 100f;
  public bool frozen;
  public bool destroyed = false;
  public bool crashed = false;
  public bool stalled = false;
  public Rigidbody2D body;
  public Transform frontBumper;
  public LayerMask whatIsObstacle;
  public GunnerController gunner;
  public GameController gameController;
  public AudioSource audioSourceDriver;
  public GameObject tankWreck;
  public UIController ui;

  // Use this for initialization
  void Start()
  {
    ui.UpdateLife(gameObject.name, life);
    if (name != "Player1")
    {
      FlipCharacter();
      frozen = true;
    }
  }

  // Update is called once per frame
  void Update()
  {
    ReadUserInput();
  }

  void FixedUpdate()
  {
    if (life > 0)
    {
      if (!frozen) UpdateMovement();
    }
    else if (!destroyed)
    {
      destroyed = true;
      GameObject tankWreckInstance = Instantiate(tankWreck, transform.position, transform.rotation); // Independent from instantiator GameObject
      gameController.SetGameOver(gameObject.name);
      gameObject.SetActive(false);
    }
  }

  private void ReadUserInput()
  {
    move = Input.GetAxis("Horizontal");
  }

  private void UpdateMovement()
  {
    crashed = Physics2D.OverlapCircle(frontBumper.position, bumperRadius, whatIsObstacle);
    if (crashed)
    {
      StartCoroutine(Stall());
    }
    if (body.velocity.magnitude < maxVelocity && !crashed && !stalled)
    {
      body.AddForce(new Vector2(move * power, 0));
    }
    SetCharacterOrientation();
  }

  IEnumerator Stall()
  {
    stalled = true;
    yield return new WaitForSeconds(1.5f);
    stalled = false;
  }

  private void SetCharacterOrientation()
  {
    // Flip sprite
    if (move > 0.0f && !facingRight)
    {
      FlipCharacter();
    }
    else if (move < 0.0f && facingRight)
    {
      FlipCharacter();
    }
  }

  private void FlipCharacter()
  {
    facingRight = !facingRight;
    Vector3 theScale = transform.localScale;
    theScale.x *= -1;
    transform.localScale = theScale;
    NotifyGunner(facingRight);
  }

  private void NotifyGunner(bool facingRight)
  {
    gunner.SetFacingRight(facingRight);
  }

  public void Freeze()
  {
    frozen = true;
  }

  public void Unfreeze()
  {
    frozen = false;
  }

  public void TakeDamage(float damage)
  {
    life -= damage;
    if (life < 0.0f) ui.UpdateLife(gameObject.name, 0);
    else ui.UpdateLife(gameObject.name, life);
  }
}
