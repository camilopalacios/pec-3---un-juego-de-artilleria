﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{

  public void PlayGame()
  {
    SceneManager.LoadScene("GameScene");
  }

  public void PlayMadness()
  {
    SceneManager.LoadScene("MadnessScene");
  }

  public void ExitGame()
  {
    Debug.Log("Closing the application...");
    Application.Quit();
  }
}
