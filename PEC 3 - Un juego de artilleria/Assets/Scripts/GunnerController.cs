﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunnerController : MonoBehaviour
{
  private float move;
  private float turnRate = 2.0f;
  public bool facingRight = true;
  public Transform armorTransform;
  public bool shoot = false;
  public GameObject shell;
  public Transform endOfBarrel;
  public GameController gameController;
  public bool frozen;
  public AudioSource audioSourceCannon;
  public UIController ui;

  // Update is called once per frame
  void Update()
  {
    ReadUserInput();
  }

  void FixedUpdate()
  {
    if (!frozen)
    {
      UpdateUI();
      UpdateMovement();
      if (shoot) Shoot();
    }
  }

  private void ReadUserInput()
  {
    move = Input.GetAxis("Vertical");
    shoot = Input.GetButtonDown("Jump");
  }

  private void Shoot()
  {
    GameObject shot = Instantiate(shell, endOfBarrel.position, endOfBarrel.rotation); // Independent from instantiator GameObject
    audioSourceCannon.Play();
    gameController.SetCameraObjective(shot.transform);
    Destroy(shot, 20f); // If the projectile doesn't explode for some reason it is destroyed after ten seconds.
    gameController.EndOfTurn(gameObject.name);
  }

  private void UpdateUI()
  {
    float angle = Mathf.Round(Mathf.Abs(Mathf.DeltaAngle(armorTransform.eulerAngles.z, transform.eulerAngles.z)));
    if (gameObject != null) ui.UpdateAngle(gameObject.name, angle);
  }

  private void UpdateMovement()
  {
    float angle = Mathf.DeltaAngle(armorTransform.eulerAngles.z, transform.eulerAngles.z);
    if (facingRight)
    {
      if (move > 0 && angle < 90)
      {
        Quaternion targetRotation = Quaternion.FromToRotation(transform.up, -transform.right) * transform.rotation;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, turnRate * 30 * Time.deltaTime);
      }
      else if (move < 0 && angle > 0)
      {
        Quaternion targetRotation = Quaternion.FromToRotation(transform.up, transform.right) * transform.rotation;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, turnRate * 30 * Time.deltaTime);
      }
    }
    else
    {
      if (move > 0 && angle > -90)
      {
        Quaternion targetRotation = Quaternion.FromToRotation(transform.up, transform.right) * transform.rotation;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, turnRate * 30 * Time.deltaTime);
      }
      else if (move < 0 && angle < 0)
      {
        Quaternion targetRotation = Quaternion.FromToRotation(transform.up, -transform.right) * transform.rotation;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, turnRate * 30 * Time.deltaTime);
      }
    }
  }

  public void SetFacingRight(bool isFacingRight)
  {
    facingRight = isFacingRight;
  }

  public void Freeze()
  {
    frozen = true;
  }

  public void Unfreeze()
  {
    frozen = false;
  }
}
