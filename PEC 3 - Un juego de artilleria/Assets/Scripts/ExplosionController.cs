﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{
  private GameController gameController;

  void Start()
  {
    gameController = GameObject.Find("GameController").GetComponent<GameController>();
    StartCoroutine(StopEffector());
  }

  IEnumerator StopEffector()
  {
    yield return new WaitForSeconds(0.4f);
    GetComponent<PointEffector2D>().enabled = false;
  }

  void OnTriggerEnter2D(Collider2D col)
  {
    if (col.tag == "Player")
    {
      gameController.PlayerTakeDamage(col.gameObject.name);
    }
  }
}
