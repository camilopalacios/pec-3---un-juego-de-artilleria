﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellController : MonoBehaviour
{
  private GameController gameController;
  public GameObject explosion;

  // Use this for initialization
  void Start()
  {
    GetComponent<Rigidbody2D>().AddForce(transform.up * 230);
    gameController = GameObject.Find("GameController").GetComponent<GameController>();
    gameController.SetShotFired(true);
  }

  void FixedUpdate()
  {
    transform.up = GetComponent<Rigidbody2D>().velocity;
  }

  void OnCollisionEnter2D(Collision2D collision)
  {
    GameObject.Destroy(gameObject);
    GameObject explosionInstance = Instantiate(explosion, transform.position, transform.rotation); // Independent from instantiator GameObject
    GameObject.Destroy(explosionInstance, 10);
  }

  void OnDestroy()
  {
    if (gameController) gameController.SetTurnToNextPlayer();
  }
}
