﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIController : MonoBehaviour
{

  public TextMeshProUGUI player1Life, player2Life, player1Angle, player2Angle, turnTitle;

  public void UpdateLife(string player, float value)
  {
    if (player == "Player1")
    {
      player1Life.text = "HEALTH: " + value.ToString();
    }
    else player2Life.text = "HEALTH: " + value.ToString();
  }

  public void UpdateAngle(string player, float angle)
  {
    if (player == "Player1")
    {
      player1Angle.text = "CANNON: " + angle.ToString() + "º";
    }
    else player2Angle.text = "CANNON: " + angle.ToString() + "º";
  }

  public void UpdateTurnTitle(string player)
  {
    turnTitle.text = "TURN FOR " + player;
  }
}
