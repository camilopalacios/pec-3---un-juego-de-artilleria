﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankDestructionScript : MonoBehaviour
{
  void Start()
  {
    StartCoroutine(StopEffector());
  }

  IEnumerator StopEffector()
  {
    yield return new WaitForSeconds(0.4f);
    GetComponent<PointEffector2D>().enabled = false;
  }
}
