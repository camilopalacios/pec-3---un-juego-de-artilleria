﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
  public bool shotFired, turnForPlayer1; // TODO: make private
  private Transform cameraObjective;
  public CameraController cameraController;
  public GameObject[] players;
  public UIController ui;

  // Use this for initialization
  void Start()
  {
    turnForPlayer1 = true;
    players = GameObject.FindGameObjectsWithTag("Player");

  }

  // Update is called once per frame
  void Update()
  {
    if (shotFired)
    {
      FreezePlayers();
    }
    else
    {
      if (turnForPlayer1)
      {
        ui.UpdateTurnTitle("PLAYER 1");
        Unfreeze(players[1]);
      }
      else
      {
        ui.UpdateTurnTitle("PLAYER 2");
        Unfreeze(players[0]);
      }
    }
  }

  public void SetCameraObjective(Transform objective)
  {
    cameraController.SetCameraObjective(objective);
  }

  public void SetTurnToNextPlayer()
  {
    shotFired = false;
    if (turnForPlayer1)
    {
      StartCoroutine(SetCameraToPlayer(players[1].transform));
      // cameraController.SetCameraObjective(players[1].transform);
    }
    else
    {
      StartCoroutine(SetCameraToPlayer(players[0].transform));
      // cameraController.SetCameraObjective(players[0].transform);
    }
  }

  IEnumerator SetCameraToPlayer(Transform player)
  {
    yield return new WaitForSeconds(3f);
    SetCameraObjective(player);
  }

  public void EndOfTurn(string name)
  {
    turnForPlayer1 = !(name == "Player1");
  }

  public void SetShotFired(bool isFired)
  {
    shotFired = isFired;
  }

  private void FreezePlayers()
  {
    Freeze(players[0]);
    Freeze(players[1]);
  }

  private void Freeze(GameObject player)
  {
    player.GetComponent<DriverController>().Freeze();
    player.GetComponentInChildren<GunnerController>().Freeze();
  }

  private void Unfreeze(GameObject player)
  {
    player.GetComponent<DriverController>().Unfreeze();
    player.GetComponentInChildren<GunnerController>().Unfreeze();
  }

  public void PlayerTakeDamage(string name)
  {
    if (name == "Player1") players[1].GetComponent<DriverController>().TakeDamage(30);
    else players[0].GetComponent<DriverController>().TakeDamage(30);
  }

  public void SetGameOver(string loser)
  {
    PlayerPrefs.SetString("Loser", loser);
    StartCoroutine(GameOver());
  }

  IEnumerator GameOver()
  {
    yield return new WaitForSeconds(4f);
    SceneManager.LoadScene("GameOver");
  }
}
