# PEC 3 - Un juego de artillería - MONSTER MAIN BATTLE TANK ADVENTURE

Tercera práctica de la asignatura de programación en Unity 2D. En ella se implementa un juego de disparos en 2D al estilo del juego clásico Scorched Earth y de la famosa saga Worms.

## Descripción

Basado en los juegos comentados anteriormente, MONSTER MAIN BATTLE TANK ADVENTURE nos transporta a un mundo montañoso en 2D y nos pone al mando de un tanque de guerra. Al otro lado del valle se encuentra nuestro rival, también al mando de un tanque. El objetivo del juego es destruir al rival antes de que el haga lo propio.
El objetivo no es trivial ya que nos separa una larga distancia, el objetivo no parará de moverse y sólo tenemos como referencia el ángulo de inclinación del cañón de nuestro tanque respecto a la torreta.

Este juego está pensado para dos jugadores que deberán turnarse en una misma máquina.

Además del modo convencional, se ha incluido un modo 'MADNESS' en el que los dos tanques se enfrentan al pie del valle con una mínima distancia entre ellos y ningún lugar dónde esconderse.

## Cómo jugar

- Para desplazar horizontalmente el tanque el jugador debe utilizar las teclas **A** y **D** del teclado, o las teclas de flecha **IZQUIERDA** y **DERECHA**.
- Para graduar la inclinación del cañón se utilizan las teclas **W** y **S** o las teclas de flecha **ARRIBA** y **ABAJO**.
- Para disparar el arma se utiliza la tecla **ESPACIO**

Cada jugador dispone de un único disparo por turno. Una vez hecho el disparo, si el rival ha sobrevivido, tendrá el turno de atacar. El HUD permite al jugador saber en todo momento si es su turno de atacar, el estado en el que se encuentra el tanque en una escala de 0 a 100 y el ángulo de inclinación del cañón respecto a la torreta.
En [este video](https://youtu.be/uWLSlwSd77Y) se puede ver una partida de prueba, tanto en el modo NORMAL, como en el modo MADNESS.


## Estructura del proyecto

El proyecto está estructurado de la manera clásica de un proyecto sencillo de Unity. La carpeta **Assets** contiene todos los recursos del juego separados por tipos:

- **Animations:** contiene las animaciones de los personajes.
- **Audio:** contiene los audios del proyecto.
- **Materials:** contiene materiales usados en Unity, en el caso de este proyecto, para la creación de partículas de humo.
- **Prefabs:** contiene elementos del juego que se repiten y/o queremos generar durante runtime.
- **Scenes:** contiene las escenas del proyecto.
- **Scripts:** contiene los scripts del proyecto.
- **Sprites:** contiene los sprites del proyecto.

Como novedad en esta PEC respecto a las anteriores se incluye el uso de particulas para simular el humo que desprenden los proyectiles a medida que se desplazan por el aire, y de manera similar el de las explosiones, al impactar el proyectil contra algo o al explotar uno de los tanques. 

## Clases principales

### MenuController

Controla la lógica para iniciar la partida o salir del juego.

### EndController

Muestra un mensaje de **GAME OVER** diciéndonos qué jugador ha sido derrotado, y la música de final de partida. Automáticamente redirige al jugador al menú principal pasado unos segundos.

### GameController

Es la clase principal del juego y gestiona el flujo de la partida. Es el encargado de cargar la escena de **GAME OVER** pasando el valor del jugador que ha sido derrotado.

### CameraController

Se encarga de seguir al jugador que tiene el turno por el escenario. Cuando el jugador realiza un disparo la cámara sigue al proyectil por el aire hasta que explote. Unos segundos después pasará seguir al siguiente jugador.

### UIController

Ofrece métodos para actualizar los diferentes campos de la UI durante la partida (Health, Angle, Turn).

### MusicController

Gestiona la música de fondo. Recibe órdenes del GameController para silenciar la música si el usuario pulsa la tecla 'M'.

### DriverController

Contiene la lógica para gestionar el movimiento del tanque.

### GunnerController

Contiene la lógica para gestionar el cañón principal del tanque.

### ShellController

Contiene la lógica para gestionar al proyectil, una vez que ha sido creado y lanzado. Esta clase se encarga de destruir su instancia cuando el proyectil impacta con algo, instanciando una explosión en su lugar.

### ExplosionController

Contiene la lógica para gestionar la explosión del proyectil. Instancia una serie de escombros y partículas de humo que desaparecen al cabo de unos segundos.

## Notas del autor (trabajo futuro)

Muy a mi pesar no me ha sido posible implementar varios aspectos importantes que quería incluir en esta práctica. Principalmente el ofrecer un modo de un jugador enfrentándose a una IA. Otros aspectos que me parecía muy interesantes siguiendo lo aprendido en los juegos Worms son la destrucción en el terreno, graduar la potencia en el disparo, efectos climatológicos cambiantes, power-ups en las armas y el blindaje, etc.